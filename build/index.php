<?
$request = explode('/', $_SERVER['REQUEST_URI']);
switch ($request[1]) {
  case '':
  case 'stream':
    main();
  break;
  case 'post':
    $request[2] ? main() : redirect();
  break;
  case 'admin':
    if ($request[2]) {
      include 'login.php';
    } else {
      $admin = "<script src='js/admin.js'></script>" . PHP_EOL;
      main();
    }
  break;
  default:
    redirect();
  break;
}
function main() {
  header('Last-Modified:' . date(r, filemtime('main.html')));
  include 'main.html';
}
function redirect() {
  header("Location: /post/404.php");
}
?>