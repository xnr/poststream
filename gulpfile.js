var csso = require('gulp-csso'),
    uglify = require('gulp-uglify'),
    concat = require('gulp-concat'),
    templateCache = require('gulp-angular-templatecache'),
    image = require('gulp-image'),
    gulp = require('gulp');

gulp.task('js', ['tmpl'], function() {
  return gulp.src([
    './app/js/*.js',
    '!./app/js/admin*.js'
  ])
    .pipe(concat('main.js'))
    .pipe(uglify())
    .pipe(gulp.dest('./build/js'));
});
gulp.task('tmpl', function() {
  return gulp.src([
    './app/**/*.html',
    '!./app/index.html',
    '!./app/html/admin/*.html'
  ])
    .pipe(templateCache({standalone: true}))
    .pipe(gulp.dest('./app/js'))
});
gulp.task('admin', ['admin_tmpl'], function() {
  return gulp.src('./app/js/admin*.js')
    .pipe(concat('admin.js'))
    .pipe(uglify())
    .pipe(gulp.dest('./build/js'));
});
gulp.task('admin_tmpl', function() {
  return gulp.src([
    './app/**/*.html',
    '!./app/index.html',
    '!./app/html/*.html'
  ])
    .pipe(templateCache({filename: 'admin_tmpl.js'}))
    .pipe(gulp.dest('./app/js'))
});
gulp.task('img', function() {
  return gulp.src('./app/img/*.png')
    .pipe(image())
    .pipe(gulp.dest('./build/img'))
});
gulp.task('css', function() {
  return gulp.src('./app/css/*.css')
    .pipe(csso())
    .pipe(gulp.dest('./build/css'))
});