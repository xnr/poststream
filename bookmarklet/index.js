window._poster_?_poster_.buildWindow():_poster_=new function(){
	var self=this,
		link=document.URL,
		html=false,
		css='{display:block;position:fixed;overflow:hidden;z-index:1999999999;top:8px;right:8px;width:400px;height:36px;transition:height .3s;box-shadow:-1px 1px 14px rgba(0,0,0,0.7);outline:4px solid #cfd8dc;}',
		style=document.createElement('STYLE');
	this.src='http://poststream.pp.ua';
	this.id='i'+Math.floor(Math.random()*1000000);
	this.buildWindow=function(){
		if(self.window)return;
		if(!setHTML())return;
		var frame=document.createElement('IFRAME');
		frame.setAttribute('frameBorder','0');
		frame.src=self.src+'/bookmarklet/iframe.html';
		frame.id=self.id;
		frame.onload=self.refresh;
		document.body.appendChild(frame);
		document.body.onmousedown=self.minimize;
		document.body.onmouseup=self.refresh;
		self.window=document.getElementById(self.id);
	}
	this.close=function(){
		document.body.removeChild(self.window);
		self.window=document.body.onmousedown=document.body.onmouseup=null;
	}
	this.refresh=function(){
		self.window.style.height='360px';
		if(setHTML())self.window.contentWindow.postMessage(contentFormat(),'*');
	}
	this.minimize=function(){
		self.window.style.height='36px';
	}
	style.type="text/css";
	style.styleSheet?style.styleSheet.cssText='#'+this.id+css:style.appendChild(document.createTextNode('#'+this.id+css));
	document.getElementsByTagName('HEAD')[0].appendChild(style);
	window.addEventListener?window.addEventListener("message",handler,false):window.attachEvent("onmessage",handler);
	this.buildWindow();
	function handler(e){
		if(e.origin!=self.src)return;
		if(self[e.data])self[e.data]();
	}
	function setHTML(){
		html=window.getSelection?content():iecontent();
		return html;
	}
	function content(){
		if(window.getSelection().toString().length==0)return false;
		return window.getSelection().getRangeAt(0).cloneContents();
	}
	function iecontent(){
		if(document.selection.createRange().text.length==0)return false;
		return document.selection.createRange();
	}
	function contentFormat(){
		var tmp=document.createElement('div'),
			notag=['STYLE','SCRIPT','NOSCRIPT','NOEMBED','LINK','META'];
		tmp.appendChild(html);//IE8 error
		var node=tmp.getElementsByTagName('*');
		for(var i=node.length-1;i>=0;i--){
			if(node[i].tagName=='EMBED')continue;
			if(notag.indexOf(node[i].tagName)+1){
				node[i].parentNode.removeChild(node[i]);
				continue;
			}
			var attr=node[i].attributes,
				length=attr.length;
			for(var j=length-1;j>=0;j--){
				switch(attr[j].name){
					case 'href':node[i].setAttribute('href',node[i].href);
					break;
					case 'src':node[i].setAttribute('src',node[i].src);
					break;
					case 'frameborder':continue;
					break;
					default:node[i].removeAttribute(attr[j].name);
					break;
				}
			}
			for(var j in node[i].childNodes)if(node[i].childNodes[j].nodeType==3)node[i].childNodes[j].data=node[i].childNodes[j].data.replace(/'/g,"’");
		}
		return JSON.stringify?JSON.stringify({html:encodeURIComponent(tmp.innerHTML),link:link,date:+new Date()}):JSON.encode({html:encodeURIComponent(tmp.innerHTML),link:link,date:+new Date()});
	}
}