'use strict';

angular.module('poststream', ['ui.router', 'ngMaterial', 'info', 'tag', 'stream', 'post', 'templates']).
  config(config).
  service('fusion', fusion).
  factory('emptyObject', emptyObject).
  value('streamTags', {}).
  constant('streamConfig', {
    'tagTable': '1xnCuwvwoT3DYyJ4ZmSGmNBdnzhCR8bUG1JQxhBE',
    'mainTable': '1_vP7nWkxzcJhP9rphpBtZGihu4Z-MYoozpn5FaY',
    'color': ['red', 'pink', 'purple', 'deep-purple', 'indigo', 'blue', 'light-blue', 'cyan', 'teal', 'green', 'light-green', 'lime', 'yellow', 'amber', 'orange', 'deep-orange']
  }).
  filter('decodeURIComponent', function() {
    return window.decodeURIComponent;
  });
config.$inject = ['$mdThemingProvider', '$locationProvider', '$stateProvider', '$urlRouterProvider'];
fusion.$inject = ['toast'];

function mainLoad() {
  angular.bootstrap(document, ['poststream'], {strictDi: true});
}
function config($mdThemingProvider, $locationProvider, $stateProvider, $urlRouterProvider) {
  $mdThemingProvider.theme('default').primaryPalette('blue-grey');
  $mdThemingProvider.theme('error');
  $locationProvider.html5Mode(true);
  $urlRouterProvider.otherwise('/stream/');
  $stateProvider.state('stream', {
    url: '/stream/{tagId}',
    templateUrl: 'html/stream.html',
    controller: 'streamCtrl',
    controllerAs: 'stream'
  }).state('stream.post', {
    url: '^/post/{postId}',
    controller: 'postCtrl'
  });
}
function fusion(toast) {
  return {
    select: function(sqlParam, callback) {
      var columns = sqlParam.columns || "*",
          sql = "SELECT " + columns + " FROM "+ sqlParam.table;
      if (sqlParam.filter) sql += " WHERE date IN ("+sqlParam.filter+")";
      if (sqlParam.limit) sql += " ORDER BY date DESC OFFSET "+sqlParam.offset+" LIMIT "+sqlParam.limit;
      gapi.client.request({
        path: "fusiontables/v2/query",
        params: {
          "key": "AIzaSyCOMl1Kvw2F-df6GDMCYvOSCZoQ9zFU7DM",
          "sql": sql
        },
        method: "GET",
        callback: function(resp) {
          if (resp.error) {
            toast.error(resp.error.message);
            //return;
          }
          callback.call(this, resp);
        }
      })
    }
  }
}
function emptyObject() {
  return function(object) {
    var empty = true;
    for (var key in object) {
      if (object.hasOwnProperty(key)) {
        empty = false;
        break;
      }
    }
    return empty;
  }
}