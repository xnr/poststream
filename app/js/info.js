'use strict';

angular.module('info', []).
  service('toast', toast);
toast.$inject = ['$mdToast'];

function toast($mdToast) {
  function simpleToast(text) {
    return $mdToast.simple().textContent(text).hideDelay(6000);
  }
  return {
    error: function(text) {
      $mdToast.show(
        simpleToast(text).theme('error')
      )
    },
    message: function(text) {
      $mdToast.show(
        simpleToast(text)
      )
    }
  }
}