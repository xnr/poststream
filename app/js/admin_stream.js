'use strict';

angular.module('stream').
  controller('adminStreamCtrl', adminStreamCtrl).
  directive('adminLoginButton', adminLoginButton);
adminStreamCtrl.$inject = ['$http', 'adminFusion', 'toast'];
adminLoginButton.$inject = ['adminFusion', '$timeout'];

function adminStreamCtrl($http, adminFusion, toast) {
  var self = this;
  //this.src = true; test. must be deleted
  this.success = true;
  this.login = function() {
    if (!self.success) {
      adminFusion.auth(false, self.authResult);
    }
  }
  this.authResult = function(result) {
    if (result && !result.error) {
      self.success = true;
      loginValidation();
    } else {
      self.success = false;
    }
  }
  function loginValidation() {
    gapi.client.load('plus', 'v1', function() {
      var request = gapi.client.plus.people.get({
        'userId':'me'
      });
      request.execute(function(resp) {
        $http.get('/admin/' + resp.id).then(function(response) {
          var data = response.data || 0;
          if (!(data && data.admin)) {
            toast.error('Ошибка авторизации. Вы не администратор');
            return false;
          }
          self.src = resp.image.url;
        }, function() {
          toast.error('Ошибка авторизации. Сетевая ошибка');
        });
      });
    });
  }
}
function adminLoginButton(adminFusion, $timeout) {
  return {
    templateUrl: 'html/admin/login_button.html',
    controller: 'adminStreamCtrl',
    controllerAs: 'adminStream',
    link: function(scope, elem, attrs) {
      gapi.client.setApiKey('AIzaSyCOMl1Kvw2F-df6GDMCYvOSCZoQ9zFU7DM');
      $timeout(function() {
        adminFusion.auth(true, scope.adminStream.authResult);
      }, 1);
    }
  }
}