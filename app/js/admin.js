'use strict';

window.admin = true;
angular.module('poststream').
  service('adminFusion', adminFusion);
adminFusion.$inject = ['toast'];

function adminFusion(toast) {
  function request(sql, callback) {
    var body = "sql=" + encodeURIComponent(sql);
    gapi.client.request({
      path: 'fusiontables/v1/query',
      method: 'POST',
      callback: function(resp) {
        if (resp.error) {
          toast.error(resp.error.message);
          return;
        }
        callback.call(this, resp);
      },
      headers: {'Content-Type':'application/x-www-form-urlencoded'},
      body: body
    });
  }
  function auth(immediate, callback) {
    gapi.auth.authorize(
      {client_id: '268044165800-enspalo09k5ikffmksdrb0ceuegkje8c.apps.googleusercontent.com',
      scope: 'https://www.googleapis.com/auth/fusiontables',
      immediate: immediate},
      callback
    );
  }
  function insert(sqlParamArray, callback) {
    var sqlParam = [];
    sqlParamArray.forEach(function(item) {
      sqlParam.push("INSERT INTO "+ item.table + " (" + item.rows + ") VALUES (" + item.values + ")");
    });
    auth(true, request(
      sqlParam.join(";"),
      callback
    ));
  }
  function update(sqlParam, callback) {
    auth(true, request(
      "UPDATE " + sqlParam.table + " SET " + sqlParam.column + "='" + sqlParam.value + "' WHERE ROWID='" + sqlParam.rowid + "'",
      callback
    ));
  }
  function del(sqlParam, callback) {
    auth(true, request(
      "DELETE FROM " + sqlParam.table + " WHERE ROWID='" + sqlParam.rowid + "'",
      callback
    ));
  }
  return {
    auth: auth,
    insert: insert,
    update: update,
    del: del
  }
}