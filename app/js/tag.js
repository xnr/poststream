'use strict';

angular.module('tag', []).
  component('streamTags', {
    templateUrl: 'html/tags.html',
    controller: tagCtrl,
    controllerAs: 'tags'
  });
tagCtrl.$inject = ['$rootScope', 'streamTags', 'fusion', 'streamConfig'];

function tagCtrl($rootScope, streamTags, fusion, streamConfig) {
  var self = this;
  this.array = [];
  fusion.select({
    table: streamConfig.tagTable,
    columns: "tag,date,rowid"
  }, function(resp) {
    var biggest = 1;
    for (var i in resp.rows) {
      streamTags[resp.rows[i][0]] = [resp.rows[i][1], resp.rows[i][2]];
      self.array.push({
        "text": resp.rows[i][0],
        "value": resp.rows[i][1],
        "size": resp.rows[i][1].split(" ").length
      });
      biggest = Math.max(biggest, resp.rows[i][1].split(" ").length);
    }
    self.biggest = biggest;
    $rootScope.$emit('getTags');
  });
};