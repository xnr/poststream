'use strict';

window.admin = true;
angular.module('post').
  controller('adminPostCtrl', adminPostCtrl).
  directive('adminPostButtons', adminPostButtons).
  directive('adminPostContent', adminPostContent);
adminPostCtrl.$inject = ['$scope', 'streamTags', 'emptyObject', 'adminFusion', 'streamConfig', 'toast'];

function adminPostCtrl($scope, streamTags, emptyObject, adminFusion, streamConfig, toast) {
  var self = this;
  this.editor = function() {
    //function for edit post HTML-code
  }
  this.saver = function() {
    var saveTags = prepareTags();
    if (!saveTags) {
      toast.message('нет изменений для сохранения');
      return;
    }
    if (saveTags.insert) {
      var sqlParamArray = [];
      saveTags.insert.forEach(function(item) {
        sqlParamArray.push({
          table: streamConfig.tagTable,
          rows: "tag,date",
          values: "'" + item.tag + "','" + item.date + "'"
        });
      });
      adminFusion.insert(sqlParamArray, $scope.post.close);
    }
    if (saveTags.del) {
      saveTags.del.forEach(function(item) {
        adminFusion.del({
          table: streamConfig.tagTable,
          rowid: item.rowid
        }, $scope.post.close);
      });
    }
    if (saveTags.update) {
      saveTags.update.forEach(function(item) {
        adminFusion.update({
          table: streamConfig.tagTable,
          rowid: item.rowid,
          column: "date",
          value: item.date
        }, $scope.post.close);
      });
    }
  }
  function prepareTags() {
    var insert = [],
        update = [],
        del = [],
        id = $scope.post.tile[0].id,
        result = {};
    $scope.post.tags.forEach(function(item) {
      var addTag = {};
      if (streamTags.hasOwnProperty(item)) {
        if (streamTags[item][0].indexOf(id)+1) {
          return;
        } else {
          var date = streamTags[item][0].split(' ');
          date.push(id);
          addTag.date = date.join(' ');
          addTag.rowid = streamTags[item][1];
          update.push(addTag);
        }
      } else {
        addTag.tag = item;
        addTag.date = id;
        insert.push(addTag);
      }
    });
    for (var streamTag in streamTags) {
      var delTag = {
        rowid: streamTags[streamTag][1]
      };
      if (streamTags[streamTag][0].indexOf(id)+1 && !($scope.post.tags.indexOf(streamTag)+1)) {
        var date = streamTags[streamTag][0].split(' ').filter(function(item) {
          return item != id;
        });
        if (date.length) {
          delTag.date = date.join(' ');
          update.push(delTag);
        } else {
          del.push(delTag);
        }
      }
    }
    if (insert.length) result.insert = insert;
    if (update.length) result.update = update;
    if (del.length) result.del = del;
    if (emptyObject(result)) return false;
    return result;
  }
}
function adminPostButtons() {
  return {
    templateUrl: 'html/admin/post_buttons.html',
    controller: 'adminPostCtrl',
    controllerAs: 'adminPost'
  }
}
function adminPostContent() {
  return {
    templateUrl: 'html/admin/post_content.html'
  }
}