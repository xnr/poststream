'use strict';

angular.module('stream', []).
  controller('streamCtrl', streamCtrl).
  directive('streamScroll', streamScroll).
  directive('streamPostContent', streamPostContent).
  directive("streamPostClickDelegate", streamPostClickDelegate);
streamCtrl.$inject = ['$scope', '$rootScope', '$state', '$stateParams', '$mdSidenav', 'streamTags', 'fusion', 'streamConfig', 'emptyObject'];
streamScroll.$inject = ['$rootScope'];
streamPostClickDelegate.$inject = ['$parse'];

function streamCtrl($scope, $rootScope, $state, $stateParams, $mdSidenav, streamTags, fusion, streamConfig, emptyObject) {
  var self = this,
      limit = 20,
      offset = 0,
      tag = $stateParams.tagId || '';
  this.stateTag = tag;
  this.tiles = [];
  this.busy = 0;
  this.getPage = function() {
    if (self.busy) return;
    if ($scope.post) $scope.post.close();
    $mdSidenav('left').close();
    self.busy = 1;
    var sqlParam = {
      table: streamConfig.mainTable,
      offset: offset,
      limit: limit
    };
    if (self.filter) {
      sqlParam.filter = self.filter.replace(/\s/g, ',');
    };
    fusion.select(sqlParam, function(resp) {
      self.busy = 0;
      if (!resp.rows) {
        $scope.$apply(function() {
          self.getPage = null;
        });
        return;
      }
      for (var i in resp.rows) {
        var tile = {
          "id": resp.rows[i][2],
          "html": resp.rows[i][0],
          "src": resp.rows[i][1],
          "color": streamConfig.color[ ~~(Math.random() * (streamConfig.color.length-1)) ]
        };
        $scope.$apply(function() {
          self.tiles.push(tile);
        });
      }
      offset += limit;
    });
  };
  this.getPost = function(postId) {
    $state.go('stream.post', {postId: postId});
  };
  this.toggleTags = function() {
    $mdSidenav('left').toggle();
  };
  if (emptyObject(streamTags)) {
    $rootScope.$on('getTags', function() {
      if (tag) self.filter = streamTags[tag][0];
      self.getPage();
    });
    return;
  }
  if (tag) this.filter = streamTags[tag][0];
  this.getPage();
}
function streamScroll($rootScope) {
  return function(scope, elem, attrs) {
    var $elem = angular.element(elem);
    $elem.on('scroll', scrollHandler);
    scope.$on('$destroy', function() {
      return $elem.off('scroll', scrollHandler);
    });
    function scrollHandler() {
      if (+attrs.streamScrollDisabled) return;
      var mdContent = document.getElementsByTagName('md-content')[0];
      if (mdContent.scrollHeight - mdContent.scrollTop <= document.documentElement.clientHeight * 2) {
        return $rootScope.$$phase ? scope.$eval(attrs.streamScroll) : scope.$apply(attrs.streamScroll);
      }
    }
  }
}
function streamPostContent() {
  return function(scope, elem, attrs) {
    var tmpDiv = angular.element('<div/>');
    tmpDiv.html(attrs.inner);
    var video = tmpDiv.find('iframe')[0];
    var image = tmpDiv.find('img')[0];
    var header = tmpDiv.find('h1')[0] || tmpDiv.find('h2')[0] || tmpDiv.find('h3')[0] || tmpDiv.find('h4')[0];
    if (header) {
      elem.append(angular.element('<h2/>').append(angular.element(header).text()));
    }
    if (video) {
      if (video.src.search('vimeo.com') + 1) {
        elem.append(video);
      } else {
        var src = "//img.youtube.com/vi/" + video.src.replace(/^.+\/(.+)$/, "$1").replace(/\?.*/, "") + "/0.jpg";
        elem.parent().css('background-image', 'url('+src+')');
        elem.addClass('image video');
      }
    } else if (image) {
      elem.parent().css('background-image', 'url('+image.src+')');
      elem.addClass('image');
    }
    tmpDiv = null;
  }
}
function streamPostClickDelegate($parse) {
  return function($scope, elem, attrs) {
    var config = attrs.streamPostClickDelegate.split("|"),
        selector = config[0].replace(/\s$/, ""),
        expression = $parse(config[1].replace(/^\s/, ""));
    elem.on("click", function(event) {
      event.preventDefault();
      var target = event.target;
      if (!(target.className.indexOf(selector) + 1)) return;
      var localScope = angular.element(target).scope();
      localScope.$apply(function() {
        expression(localScope);
      })
    });
    $scope.$on("$destroy", function(event) {
      elem.off("click");
    });
  }
}