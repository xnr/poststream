'use strict';

angular.module('post', []).
  controller('postCtrl', postCtrl).
  directive('postContent', postContent).
  directive('postTags', postTags);
postCtrl.$inject = ['$scope', '$rootScope', '$stateParams', '$mdDialog', 'streamTags', 'emptyObject'];

function postCtrl($scope, $rootScope, $stateParams, $mdDialog, streamTags, emptyObject) {
  if (emptyObject(streamTags)) {
    $rootScope.$on('getTags', function() {
      postDialogShow($mdDialog, $scope, $stateParams, streamTags);
    });
    return;
  }
  postDialogShow($mdDialog, $scope, $stateParams, streamTags);
}
function postDialogShow($mdDialog, $scope, $stateParams, streamTags) {
  $mdDialog.show({
    controller: postDialogCtrl,
    controllerAs: 'post',
    scope: $scope,
    preserveScope: true,
    templateUrl: 'html/post.html',
    locals: {id: $stateParams.postId.replace(/\.php/g, ""), tags: streamTags}
  });
  postDialogCtrl.$inject = ['$scope', '$location', '$mdDialog', '$state', 'fusion', 'streamConfig', 'id', 'tags'];
}
function postDialogCtrl($scope, $location, $mdDialog, $state, fusion, streamConfig, id, tags) {
  var self = this;
  this.tile = $scope.stream.tiles.filter(function(tile) {
    return tile.id == id;
  });
  if (!this.tile.length) {
    fusion.select({table: streamConfig.mainTable, filter: id}, function(resp) {
      if (!resp.rows) {
        $scope.$apply(function() {
          self.tile.push({
            "html": "<h3>Ошибка 404. Страница не найдена</h3>",
            "src": "http://" + $location.host()
          });
        });
        return;
      };
      $scope.$apply(function() {
        self.tile.push({
          "id": resp.rows[0][2],
          "html": resp.rows[0][0],
          "src": resp.rows[0][1],
          "color": streamConfig.color[ ~~(Math.random() * (streamConfig.color.length-1)) ]
        });
      });
    });
  }
  this.tags = [];
  var tmpl = new RegExp('\\b' + id + '\\b');
  for (var tag in tags) {
    if (tags[tag][0].search(tmpl) + 1) {
      this.tags.push(tag);
    }
  }
  this.close = function() {
    $mdDialog.hide();
    $state.go('stream', {tagId: $scope.stream.stateTag});
    $scope.post = null;
  };
  this.selectedTag = null;
  this.searchText = null;
  this.querySearch = function(query) {
    return query ? tagFilter(query) : [];
  }
  function tagFilter(query) {
    var result = [];
    query = angular.lowercase(query);
    for (var tag in tags) {
      if (tags.hasOwnProperty(tag) && (tag.indexOf(query) === 0)) result.push(tag);
    }
    return result;
  }
}
function postContent() {
  return function(scope, elem, attrs) {
    if (!scope.post.tile.length) {
      scope.$watch('post.tile[0]', function() {
        if (!scope.post.tile.length) return;
        var id = scope.post.tile[0].id;
        if (!scope.post.tile[0].id) {
          scope.post.tile[0].id = +new Date();
          id = "404";
        }
        inner(scope.post.tile[0].html);
        scope.post.comments = "/post.php?id=" + id;
      })
      return;
    }
    scope.post.comments = "/post.php?id=" + scope.post.tile[0].id;
    inner(scope.post.tile[0].html);

    function inner(html) {
      elem.html(decodeURIComponent(html));
    }
  }
}
function postTags() {
  return {
    templateUrl: window.admin ? 'html/admin/post_tags.html' : 'html/post_tags.html',
    replace: true
  }
}